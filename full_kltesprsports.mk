# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit from kltesprsports device
$(call inherit-product, device/samsung/kltesprsports/device.mk)

# Set those variables here to overwrite the inherited values.
PRODUCT_NAME := full_kltesprsports
PRODUCT_DEVICE := kltesprsports
PRODUCT_BRAND := samsung
PRODUCT_MANUFACTURER := samsung
PRODUCT_MODEL := kltesprsports
