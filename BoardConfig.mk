# inherit from common klte
-include device/samsung/klte-common/BoardConfigCommon.mk

TARGET_OTA_ASSERT_DEVICE := kltesprsports

# Kernel
TARGET_KERNEL_SOURCE := kernel/samsung/kltesprsports
TARGET_KERNEL_VARIANT_CONFIG := msm8974pro_sec_ksportslte_spr_defconfig
TARGET_KERNEL_TIMA_CONFIG := tima_defconfig

# Init
TARGET_INIT_VENDOR_LIB := libinit_msm
TARGET_LIBINIT_DEFINES_FILE := device/samsung/kltesprsports/init/init_klte.c
TARGET_UNIFIED_DEVICE := true

# Partitions
BOARD_BOOTIMAGE_PARTITION_SIZE := 13631488
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 15728640
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 2831155200
BOARD_USERDATAIMAGE_PARTITION_SIZE := 12187581440

# inherit from the proprietary version
-include vendor/samsung/kltesprsports/BoardConfigVendor.mk
